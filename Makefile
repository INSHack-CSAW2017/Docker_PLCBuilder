CURRENT_DIRECTORY := $(shell pwd)
NAME = plc-builder
TAG = latest
IMAGE = csaw/$(NAME)
CONTAINER = plc-builder

build:
	@docker build -t $(IMAGE):$(TAG) $(CURRENT_DIRECTORY)

remove:
	@docker rm $(CONTAINER)

compile:
ifeq ($(ST_FILE),)
	echo "Need to set ST_FILE"
else ifeq ($(OUT),)
	echo "Need to set OUT"
else
	#Check ST_FILE
	@ls $(ST_FILE)

	#Check OUT
	@touch $(OUT)

	#Create
	@docker create --name $(CONTAINER) $(IMAGE):$(TAG)

	#Set input
	@docker cp $(ST_FILE) $(CONTAINER):/build/input/program.st

	#Start
	@docker start -a $(CONTAINER)

	#GetOutput
	@docker cp $(CONTAINER):/build/output/openplc $(OUT)

	#Remove
	@docker rm $(CONTAINER)
endif
