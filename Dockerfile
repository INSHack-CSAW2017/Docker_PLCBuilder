FROM arm32v7/debian:stretch-slim

# basic flask environment
RUN apt-get update && apt-get install -y build-essential

COPY build/ /build/

# exectute start up script
CMD ["/build/build.sh"]
