# DockerPlcBuilder
DockerPlcBuilder permet de compiler un plc à partir d'un st_file

## Note
Sì vous le faites tourner depuis une architecture X86_32 bits ou ARM (raspberry) 
   vous aurez besoin de changer les binaires dans /Docker_PLCBuilder/build/bin
   
Vous prenez donc le projet OPENPLC, https://github.com/thiagoralves/OpenPLC_v2
et vous le compilez une fois sur la bonne archi

Ensuite vous récupérez les binaires dans OPENPLC
   
- OpenPLC_v2/core/gluegenerator 
- OpenPLC_v2/st_optimize_src/st_optimiser 
- OpenPLC_v2/matiec_src/iec2c
   
Et vous remplacez ceux dans les sources du conteneurs, dans `Docker_PLCBuilder/core/build/bin`

## Build

    git clone https://gitlab.com/INSHack-CSAW2017/Docker_PLCBuilder.git
    cd Docker_PLCBuilder/
    // Remplacez ici les binaires si nécessaire
    sudo make build -B

## Run

    sudo make compile ST_FILE=program.st OUT=program || sudo make remove
    
- ST_FILE: Votre chemin vers le programme en .st
- OUT: Votre fichier de sortie
- N'oubliez pas la deuxième partie de la commande, car si une erreur survient le conteneur ne sera pas supprimé.

Si vous obtenez un problème sur le fait que le conteneur est déjà lancé, faites `sudo make remove`
    
## Comment ça marche?
Nous avons dans /build toutes les ressources nécessaires afin de créer notre PLC
dans /bin :

    - st_optimizer => Prend en entrée un Fichier .st et l'optimise (on a également un fichier .st en sortie)
    - iec2c => Transforme le fichier .st en code C
    - glue_generator => Je n'ai pas tout compris, mais a priori il gère des variables dans le programme
    
On genere donc les sources en c avec les binaires ci-dessus, 
puis on les déplace dans /build/core pour les compléter.

Enfin on compile avec G++ les sources de /build/core, et on obtiens un executable, qui est notre PLC

## Qu'est-ce qui a changé avec OpenPLC V.2
OpenPLC recompilait toutes les sources a chaque fois, 
ce qui rendait la compilation très longue.

Ici les binaires qui n'ont pas besoin d'être modifés sont précompilés.

De plus il y a une nouvelle couche physique, qui écrit le contenu des GPIOS
simulés dans la mémoire partagée. Ce qui permet, une fois executé, de récupérer 
les entrées et sorties du PLC rapidement.

