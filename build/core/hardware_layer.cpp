//-----------------------------------------------------------------------------
// Copyright 2015 Thiago Alves
//
// Based on the LDmicro software by Jonathan Westhues
// This file is part of the OpenPLC Software Stack.
//
// OpenPLC is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// OpenPLC is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with OpenPLC.  If not, see <http://www.gnu.org/licenses/>.
//------
//
// This file is the hardware layer for the OpenPLC. If you change the platform
// where it is running, you may only need to change this file. All the I/O
// related stuff is here. Basically it provides functions to read and write
// to the OpenPLC internal buffers in order to update I/O state.
// Thiago Alves, Dec 2015
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define SHMKEY 2516

#define BUFFER_SIZE		1024
#define MAX_INPUT 		14
#define MAX_OUTPUT 		11
#define MAX_ANALOG_OUT	1

#define STATUS_READY    1
#define STATUS_BUSY     0

#include "ladder.h"

struct GPIOInputOutput{
    uint8_t status;

    //Booleans
    uint8_t bool_input[MAX_INPUT];
    uint8_t bool_output[MAX_OUTPUT];

    //Analog I/O
    uint16_t int_output[MAX_ANALOG_OUT];
};



#ifndef _POSIX_THREAD_PROCESS_SHARED
#error This system does not support process shared mutex
#endif

int                 ShmID;
GPIOInputOutput*    ShmPTR;

//-----------------------------------------------------------------------------
// This function is called by the main OpenPLC routine when it is initializing.
// Hardware initialization procedures should be here.
//-----------------------------------------------------------------------------
void initializeHardware()
{

    if ((ShmID = shmget(SHMKEY, sizeof(GPIOInputOutput), IPC_CREAT |  0666)) < 0) {
        //printf("Could not create shared mem.\n");
        exit(1);
    }
    
    if ((ShmPTR = shmat(ShmID, NULL, 0)) == (char *) -1) {
        //printf("Error on shmat");
        exit(1);
    }
    ShmPTR->status = STATUS_READY;
}

//-----------------------------------------------------------------------------
// This function is called by the OpenPLC in a loop. Here the internal buffers
// must be updated to reflect the actual state of the input pins. The mutex buffer_lock
// must be used to protect access to the buffers on a threaded environment.
//-----------------------------------------------------------------------------
void updateBuffersIn()
{
    pthread_mutex_lock(&bufferLock); //lock mutex
    ShmPTR->status = STATUS_BUSY;

    //INPUT
    //printf("Bool In");
	for (int i = 0; i < MAX_INPUT; i++)
	{
        //printf(" - %i %02x\n",i,ShmPTR->bool_input[i]);
		if (bool_input[i/8][i%8] != NULL) *bool_input[i/8][i%8] = ShmPTR->bool_input[i];
	}

    ShmPTR->status = STATUS_READY;
	pthread_mutex_unlock(&bufferLock); //unlock mutex
}

//-----------------------------------------------------------------------------
// This function is called by the OpenPLC in a loop. Here the internal buffers
// must be updated to reflect the actual state of the output pins. The mutex buffer_lock
// must be used to protect access to the buffers on a threaded environment.
//-----------------------------------------------------------------------------
void updateBuffersOut()
{
	pthread_mutex_lock(&bufferLock); //lock mutex
    ShmPTR->status = STATUS_BUSY;

    //printf("Bool Out");
	for (int i = 0; i < MAX_OUTPUT; i++)
	{
        if (bool_output[i/8][i%8] != NULL) ShmPTR->bool_output[i] = *bool_output[i/8][i%8];
        //printf(" - %i %02x\n",i,ShmPTR->bool_output[i]);
	}

    //ANALOG OUT (PWM)
    //printf("Int Out");
	for (int i = 0; i < MAX_ANALOG_OUT; i++)
	{
        if (int_output[i] != NULL) ShmPTR->int_output[i] = (*int_output[i] / 64) ;
        //printf(" - %i %i\n",i,ShmPTR->int_output[i] );
	}

    ShmPTR->status = STATUS_READY;
	pthread_mutex_unlock(&bufferLock); //unlock mutex
}
