#!/bin/bash

# Go to 1st working directory
cd /build/workdir || exit 1

# Optimize
echo Optimisation ........
/build/bin/st_optimizer /build/input/program.st program.st || exit 1
echo Optimisation ........ OK

# Compilation
echo Generate program ........
/build/bin/iec2c program.st || exit 1
echo Generate program ........ OK

# Send to core
mv -f POUS.c POUS.h LOCATED_VARIABLES.h VARIABLES.csv Config0.c Config0.h Res0.c /build/core/ || exit 1

# Go to core for next step
cd /build/core || exit 1

#Generate
echo Build ........
g++ -I ./lib -c Config0.c || exit 1
g++ -I ./lib -c Res0.c || exit 1

echo Generating glueVars.cpp
/build/bin/glue_generator || exit 1

echo Compiling main program
g++ *.cpp *.o -o openplc -I ./lib -lrt -lpthread -fpermissive || exit 1
echo Build ........ OK

#Move result to output folder
mv /build/core/openplc /build/output/openplc || exit 1
